Link-scraper

This script shows a web template with a text box and two buttons (Scrape and delete) you only have to paste your url into the search box and click on "Scrape", this will display all the id's, names and links/tags of the provide url. When you want to do another search just click on "delete" and start again.

- Requirements
    *** python 3.10

- Steps
    1.  Clone the project 
    2.  cd into 'link-scraper'
    3.  run `pip install -r requirements.txt` to install the required dependencies
    4.  cd into 'mysite'
    5.  run `python manage.py migrate` to apply all the migrations
    6.  run `python manage.py runserver` to start the script
    7.  go to http://127.0.0.8000/ to acces the template
    8.  paste a url into the search box and click on 'Scrape' 



